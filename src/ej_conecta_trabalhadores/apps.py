from django.apps import AppConfig


class EjConectaTrabalhadoresConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ej_conecta_trabalhadores"
