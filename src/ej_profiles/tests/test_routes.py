import random as rd
import string as s
from datetime import datetime

import pytest
from PIL import Image
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client
from io import BytesIO

from ej.testing import UrlTester
from ej_profiles import enums
from ej_users.models import User
from ej_conversations.mommy_recipes import ConversationRecipes
from ej_conversations import create_conversation
from ej_boards.models import Board


class TestRoutes(UrlTester):
    user_urls = ["/profile/edit/", "/profile/contributions/"]


def create_image(filename, size=(100, 100), image_mode="RGB", image_format="png"):
    data = BytesIO()
    Image.new(image_mode, size).save(data, image_format)
    data.name = filename
    data.seek(0)
    return data


@pytest.fixture
def logged_client(db):
    user = User.objects.create_user("email@server.com", "password")
    client = Client()
    client.force_login(user)
    return client


class TestProfileTourRedirect(ConversationRecipes):
    @pytest.fixture
    def admin_user(self, db):
        admin_user = User.objects.create_superuser("admin@test.com", "pass")
        profile = admin_user.get_profile()
        profile.completed_tour = True
        profile.save()
        admin_user.save()
        return admin_user

    @pytest.fixture
    def logged_admin(self, admin_user):
        client = Client()
        client.force_login(admin_user)
        return client

    @pytest.fixture
    def base_user(self, db):
        user = User.objects.create_user("tester@email.br", "password")
        profile = user.get_profile()
        profile.completed_tour = True
        profile.save()
        return user

    @pytest.fixture
    def base_board(self, base_user):
        board = Board.objects.create(slug="userboard", owner=base_user, description="board")
        return board

    @pytest.fixture
    def hiden_conversation(self, admin_user):
        board = Board.objects.create(slug="adminboard", owner=admin_user, description="board")
        conversation = create_conversation("foo", "conv", admin_user, board=board)
        conversation.is_hidden = True
        conversation.save()
        return conversation

    @pytest.fixture
    def first_conversation(self, base_board, base_user):
        conversation = create_conversation("bar", "conv1", base_user, board=base_board)
        conversation.is_hidden = False
        conversation.save()
        return conversation

    @pytest.fixture
    def second_conversation(self, base_board, base_user):
        conversation = create_conversation("forbar", "conv2", base_user, board=base_board)
        conversation.is_hidden = False
        conversation.save()
        return conversation

    @pytest.fixture
    def third_conversation(self, base_board, base_user):
        conversation = create_conversation("forbarbar", "conv3", base_user, board=base_board)
        conversation.is_hidden = True
        conversation.save()
        return conversation

    def test_redirect_if_tour_is_incomplete(
        self, logged_admin, base_board, hiden_conversation, first_conversation, second_conversation
    ):
        user = User.objects.create_user("test_profile_tour@email.br", "password")

        url = "/profile/"
        client = Client()
        client.force_login(user)

        response = client.get(url)

        assert response.status_code == 302
        assert "/profile/tour/" in response.url


class TestEditProfile:
    def test_user_logged_access_edit_profile(self, logged_client):
        resp = logged_client.get("/profile/edit/")
        assert resp.status_code == 200

    def test_user_logged_edit_profile_picture(self, logged_client):
        avatar = create_image("avatar.png")
        avatar_file = SimpleUploadedFile("front.png", avatar.getvalue())
        form_data = {"name": "Maurice", "profile_photo": avatar_file, "gender": 0, "race": 0}

        response = logged_client.post("/profile/edit/", form_data)
        assert response.status_code == 302 and response.url == "/profile/"
        user = User.objects.get(email="email@server.com")
        assert user.profile.profile_photo.name

    def test_user_logged_edit_profile_basic_info(self, logged_client):
        def rand_str(size):
            return "".join(rd.choices(s.ascii_lowercase, k=size))

        def gen_birth_date():
            return f"{rd.randint(1900, 2020)}-{rd.randint(1, 12)}-" f"{rd.randint(1, 28)}"

        inf_fields = [
            "name",
            "city",
            "occupation",
            "country",
            "ethnicity",
            "education",
            "political_activity",
            "biography",
            "state",
            "gender",
            "race",
            "birth_date",
        ]
        inf_values = [
            *[rand_str(15)] * 8,
            "DF",
            int(rd.choice(list(enums.Gender))),
            int(rd.choice(list(enums.Race))),
            gen_birth_date(),
        ]
        form_data = {k: v for k, v in zip(inf_fields, inf_values)}

        response = logged_client.post("/profile/edit/", form_data)
        assert (
            response.status_code == 302 and response.url == "/profile/"
        ), f"Error found using post message {form_data}"
        user = User.objects.get(email="email@server.com")

        for attr in ["gender", "race"]:
            assert getattr(user.profile, attr).value == form_data[attr]
            inf_fields.remove(attr)
        assert user.profile.birth_date == datetime.strptime(form_data["birth_date"], "%Y-%m-%d").date()
        inf_fields.remove("birth_date")

        blacklist = settings.EJ_PROFILE_EXCLUDE_FIELDS
        for attr in inf_fields:
            if attr not in blacklist:
                assert getattr(user.profile, attr) == form_data[attr], attr
